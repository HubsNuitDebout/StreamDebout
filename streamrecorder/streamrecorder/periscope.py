import time
import re
import html
from functools import reduce
import json
import os
from os import path
import shutil
import subprocess
import logging

import requests
import m3u8


logging.basicConfig(format='%(name)s %(levelname)s %(message)s')
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


URL = 'https://periscope.tv'

API_BASE = 'https://api.periscope.tv/api/v2'
API_BROADCASTS = API_BASE + '/getUserBroadcastsPublic?user_id=%(u)s&session_id=%(t)s'
API_BROADCAST = API_BASE + '/getBroadcastPublic?broadcast_id=%s'
API_DOWNLOAD = API_BASE + '/accessVideoPublic?broadcast_id=%s'


def _get(url, session=None, **kwargs):
    if session is None:
        r = requests.get(url, timeout=2, **kwargs)
    else:
        r = session.get(url, timeout=2, **kwargs)
    r.raise_for_status()
    return r


def get_user_broadcasts(username, session=None, date=None):
    if session is not None and time.time()*1000 < session['e']:
        return _get_user_broadcasts(session, date), session
    session = _get_new_session(username)
    return _get_user_broadcasts(session, date), session

def _get_new_session(username):
    r = _get(URL + '/%s' % username)

    m = re.search(r'data-store=([\'"])(?P<data>.+?)\1', r.text)
    if m is None:
        raise ValueError('no data-store in response, periscope must have'
                         'changed API')
    d = json.loads(html.unescape(m.group('data')))
    return {'u': d['UserCache']['usernames'][username.lower()],
            'e': d['SessionToken']['broadcastHistory']['expiry'],
            't': d['SessionToken']['broadcastHistory']['token']['session_id']}


def _get_user_broadcasts(session):
    r = _get(API_BROADCASTS % session)
    return r.json()['broadcasts']


def download(broadcast_id, directory, fmt='%(id)s'):
    fullpath = path.join(directory, broadcast_id)
    os.makedirs(fullpath, mode=0o750)

    s = requests.Session()

    r = _get(API_DOWNLOAD % broadcast_id, s)
    data = r.json()

    # download fragments
    for key in ('https_hls_url', 'replay_url'):
        if key in data:
            splited = data[key].split('/')
            try:
                _download_frags(s, splited[-1], '/'.join(splited[:-1]) + '/%s',
                        broadcast_id, fullpath,
                        fullspeed=data['broadcast']['state']!='RUNNING')
            except requests.HTTPError as err:
                if err.response.status_code == 404:
                    break
                else:
                    raise
            else:
                break
    else:
        logger.error('could not find the playlist URL')
        return

    try:
        r = _get(API_BROADCAST % broadcast_id, s)
        info = r.json()['broadcast']
    except requests.HTTPError:
        if r.status_code == 404:
            info = data['broadcast']
        else:
            raise

    # write the final file
    cmd = ['ffmpeg', '-i', path.join(fullpath, 'playlist.m3u8'),
           path.join(directory, fmt % info + '.mp4')]
    p = subprocess.check_call(cmd, stdout=subprocess.DEVNULL,
            stderr=subprocess.DEVNULL)
    shutil.rmtree(fullpath)

    # write the info file
    with open(path.join(directory, fmt % info + '.json')) as f:
        f.write(info.dumps())

    return info


def _download_frags(s, pl_name, base_url, broadcast_id, fullpath,
                   fullspeed=False):
    r = _get(base_url % pl_name, s)
    playlist = m3u8.loads(r.text)
    print(fullspeed)

    wait = 2  # just in case of empty playlist
    while True:
        st = time.time()
        r = _get(base_url % pl_name, s)
        pl = m3u8.loads(r.text)
        done = False
        print(len(pl.segments))
        for seg in pl.segments:
            if path.exists(path.join(fullpath, seg.uri)):
                logger.debug('frag %s already here' % seg.uri)
                continue
            logger.debug('new frag %s' % seg.uri)
            done = True
            try:
                r = _get(base_url % seg.uri, s)
            except requests.Timeout:
                # silently fail frag download timeout to keep up to date
                continue
            else:
                playlist.add_segment(seg)
                with open(path.join(fullpath, seg.uri), 'wb') as f:
                    f.write(r.content)
        if done:
            wait = .7*(len(pl.segments)*pl.segments[0].duration+time.time()-st)
        else:
            # we already got all pieces from this playlist, is the stream
            # still updating?
            r = _get(API_BROADCAST % broadcast_id, s)
            if r.json()['broadcast']['state'] != 'RUNNING':
                break
            else:
                wait *= 2
        print(wait)
        time.sleep(0 if fullspeed else wait)

    playlist.dump(path.join(fullpath, 'playlist.m3u8'))
