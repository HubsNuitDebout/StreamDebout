import http.client
import httplib2
import os
import random
import sys
import time

from apiclient.discovery import build
from apiclient.errors import HttpError
from apiclient.http import MediaFileUpload
from oauth2client.client import flow_from_clientsecrets
from oauth2client.file import Storage
from oauth2client.tools import argparser, run_flow


class Video():

    def __init__(self, path, title, description, category, keywords, privacyStatus):
        self.myPath = path
        self.myTitle = title
        self.myDescription = description
        self.myCategory = category
        self.myKeywords = keywords
        self.myPrivacyStatus = privacyStatus
    # @property
    # def auth_host_name(self):
    #     return 'http://streamdebout.duckdns.org'

    @property
    def noauth_local_webserver(self):
        return True

    # @property
    # def auth_host_port(self):
    #     return [8080,8090]

    @property
    def logging_level(self):
        return 'INFO'

    @property
    def file(self):
        return self.myPath

    @property
    def title(self):
        return self.myTitle

    @property
    def description(self):
        return self.myDescription

    @property
    def category(self):
        return self.myCategory

    @property
    def keywords(self):
        return self.myKeywords

    @property
    def privacyStatus(self):
        return self.myPrivacyStatus


httplib2.RETRIES = 1

MAX_RETRIES = 10

RETRIABLE_EXCEPTIONS = (httplib2.HttpLib2Error, IOError, http.client.NotConnected,
                        http.client.IncompleteRead, http.client.ImproperConnectionState,
                        http.client.CannotSendRequest, http.client.CannotSendHeader,
                        http.client.ResponseNotReady, http.client.BadStatusLine)

RETRIABLE_STATUS_CODES = [500, 502, 503, 504]

# The CLIENT_SECRETS_FILE variable specifies the name of a file that contains
# the OAuth 2.0 information for this application, including its client_id and
# client_secret. You can acquire an OAuth 2.0 client ID and client secret from
# the Google Developers Console at
# https://console.developers.google.com/.
# Please ensure that you have enabled the YouTube Data API for your project.
# For more information about using OAuth2 to access the YouTube Data API, see:
#   https://developers.google.com/youtube/v3/guides/authentication
# For more information about the client_secrets.json file format, see:
#   https://developers.google.com/api-client-library/python/guide/aaa_client_secrets
CLIENT_SECRETS_FILE = 'google_secrets.json'
# This variable defines a message to display if the CLIENT_SECRETS_FILE is
# missing.
MISSING_CLIENT_SECRETS_MESSAGE = ""
YOUTUBE_UPLOAD_SCOPE = 'https://www.googleapis.com/auth/youtube.upload'
YOUTUBE_API_SERVICE_NAME = 'youtube'
YOUTUBE_API_VERSION = 'v3'


def get_authenticated_service(secret_file, args):
    flow = flow_from_clientsecrets(
        secret_file,
        scope=YOUTUBE_UPLOAD_SCOPE,
        message=MISSING_CLIENT_SECRETS_MESSAGE)
    print(sys.argv[0])
    storage = Storage('%s-oauth2.json' % sys.argv[0])
    credentials = storage.get()

    if credentials is None or credentials.invalid:
        credentials = run_flow(flow, storage, args)

    return build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION,
                 http=credentials.authorize(httplib2.Http()))


def initialize_upload(youtube, options):
    body = {'snippet': {'title': options.title,
                        'description': options.description,
                        'tags': options.keywords,
                        'categoryId': options.category
                        },
            'status': {'privacyStatus': options.privacyStatus}
            }

    # Call the API's videos.insert method to create and upload the video.
    insert_request = youtube.videos().insert(
        part='snippet,status', body=body,
        media_body=MediaFileUpload(options.file, chunksize=-1, resumable=True))

    resumable_upload(insert_request)


def resumable_upload(insert_request):
    """This method implements an exponential backoff strategy to resume a
    failed upload."""

    response = None
    error = None
    retry = 0
    while response is None:
        try:
            print('Uploading file...')
            status, response = insert_request.next_chunk()
            if 'id' in response:
                print('Video id "%s" was successfully uploaded.' %
                      response['id'])
            else:
                print('The upload failed with an unexpected response: %s' % response)
        except HttpError as e:
            if e.resp.status in RETRIABLE_STATUS_CODES:
                error = 'A retriable HTTP error %d occurred:\n%s' % (e.resp.status,
                                                                     e.content)
            else:
                raise
        except RETRIABLE_EXCEPTIONS as e:
            error = 'A retriable error occurred: %s' % e

        if error is not None:
            print(error)
            retry += 1
            if retry > MAX_RETRIES:
                print('No longer attempting to retry.')

            max_sleep = 2 ** retry
            sleep_seconds = random.random() * max_sleep
            print('Sleeping %f seconds and then retrying...' %
                  sleep_seconds)
            time.sleep(sleep_seconds)


def upload(path, title, descr, cat, tags, priv_st, secret_file):
    # args = {'file':path, 'title':title, 'description':descr, 'category':cat, 'keywords':tags, 'privacyStatus':priv_st, 'logging_level': 'INFO'}
    args = Video(path, title, descr, cat, tags, priv_st)
    youtube = get_authenticated_service(secret_file, args)
    try:
        initialize_upload(youtube, args)
    except HttpError as e:
        print('An HTTP error %d occurred while uploading to '
                     'youtube:\n%s' % (e.resp.status, e.content))
