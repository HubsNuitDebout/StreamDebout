#!/usr/bin/env python3


import os
import logging
import pickle

from youtube_dl import YoutubeDL, DownloadError
from rq import Queue
from rq.decorators import job
from redis import Redis

import streamrecorder.youtubeuploader as ytup


DIRECTORY = 'downloads'
PROFILE_URL = 'https://www.periscope.tv/%s'
STREAM_URL = 'https://www.periscope.tv/w/%s'
STREAM_FILTER = '#streamdebout'
GOOGLE_SECRET_FILE = './client_secret.json'



logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

redis = Redis()
queue = Queue(connection=redis)


def ensure_directory(profile):
    fullpath = '%s/%s' % (DIRECTORY, profile)
    os.makedirs(fullpath, mode=0o750, exist_ok=True)


def scan_profiles():
    """Start scanning job for every profile. Profiles are in the
    `watch_profile` field of the redis DB."""

    profiles = pickle.loads(redis.get('watch_profiles'))
    for p in profiles:
        ensure_directory(p)
        queue.enqueue(scan_profile, p)


def scan_profile(profile):
    """Scans a profile for last stream."""
    print(profile)

    ydl = YoutubeDL({'playlistend': 1, 'logger': logger})
    try:
        infos = ydl.extract_info(PROFILE_URL % profile, False)
    except DownloadError:
        logger.error('apparently profile %s doesn\'t exist' % profile)
        return

    try:
        last_stream_id =  infos['entries'][0]['id']
    except IndexError:
        logger.error('something went wrong, no stream in response')
    else:
        if not redis.get(last_stream_id):
            redis.set(last_stream_id, True)
            queue.enqueue(download_stream, last_stream_id, infos,
                          '%s/%s' % (DIRECTORY, profile))



def download_stream(streamid, infos, directory):
    """Download the last stream if not already done."""

    outtmpl = '%s/%%(id)s.%%(ext)s' % directory
    ydl = YoutubeDL({
            'playlistend': 1,
            'outtmpl': outtmpl,
            'match-title': STREAM_FILTER,
            'logger': logger})
    try:
        print('############')
        #print(ydl.extract_info(STREAM_URL % streamid))
        ydl.download([STREAM_URL % streamid])
    except Exception:
        logger.error('something went wrong while downloading stream %s' % streamid)
    else:
        path = directory + '/' + infos['entries'][0]['id'] + '.' + infos['entries'][0]['ext']
        queue.enqueue(publish, path, infos)

def publish(path,infos):
    upload_youtube(path, infos['entries'][0]['title'])
    #profile = path.split('/')[-1]
    #for publisher in pickle.loads(redis.get(profile)):



def upload_ipfs(path):
    r = requests.post('%s/add?arg=%s' % (IPFS_API, path))
    if r.status_code != 200:
        logger.error('error while connecting to the IPFS daemon')

def upload_youtube(path, title):
    ytup.upload(path, title, '', 22, '', 'private', GOOGLE_SECRET_FILE)
