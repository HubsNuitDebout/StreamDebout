# -*- coding:utf-8 -*-


from setuptools import setup


with open('requirements.txt') as s:
    reqs = [l.strip() for l in s.readlines()]

setup(name='streamrecorder',
      version='0.1',
      packages=['streamrecorder'],
      scripts=['recordstreams'],
      install_requires=reqs,
      )
