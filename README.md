# StreamDebout 

StreamDebout est une plateforme permettant de sauvegarder et de relayer
l'ensemble des streams audios et vidéos concernants #NuitDebout. Le but est de
fournir une défense contre la censure aux utilisateurs de ces outils ainsi que
de permettre une meilleur distribution des fichiers.

Elle est construite autour d'un script python permettant de télécharger des
streams live depuis des sources (periscope.tv, mixlr.com, ...) automatiquement
basé sur des filtres, de les transcoder et enfin de les uploader sur une
destination (youtube, IPFS, peertube, ...).

## Roadmap

- [ ] finir le nouveau downloader periscope.tv
- [x] finir l'uploader youtube
- [ ] expérimenter l'upload/l'utilisation de peertube et IPFS
- [ ] affiner le post-processing (orientation de la vidéo...)
- [ ] ajouter l'enregistrement des flux audio
- [ ] faire une API renvoyant les streams en cours ou sauvegardé ainsi que
  filtres
- [x] update automatique youtube-dl
- [ ] optimisation youtube-dl (package distribué sous forme de binaire a utilisé directement (dezipé ?) et load uniquement des extracteurs nécésaires.
